package main.java.utils;

/**
 * @author lionel.mangoua
 * date: 18/08/21
 */

import main.java.config.GlobalEnums;
import main.java.Engine.DriverFactory;

public class SetEnvironmentDataUtility extends DriverFactory {

    //region <setTestEnvironment>
    public void setTestEnvironment(String environmentName) {

        switch(environmentName) {
            case "comment":
                env = GlobalEnums.Environment.COMMENT;
                setUpBaseUrl();
                break;
            case "post":
                env = GlobalEnums.Environment.POST;
                setUpBaseUrl();
                break;
            case "user":
                env = GlobalEnums.Environment.USER;
                setUpBaseUrl();
                break;
        }

        log("Set Environment Name: '" + environmentName + "' successfully", "INFO", "text");
    }
    //endregion
}
