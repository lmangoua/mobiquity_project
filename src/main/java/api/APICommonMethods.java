package main.java.api;

/**
 * @author lionel.mangoua
 * date: 18/08/20
 */

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import main.java.Engine.DriverFactory;

import java.io.File;
import java.util.*;

import static io.restassured.RestAssured.given;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static main.java.api.CustomHeaders.customHeadersMap;

@SuppressWarnings("unchecked")
public class APICommonMethods extends DriverFactory {

    public static String getValueFromJsonResp(ValidatableResponse response, String keyString) {

        String value = null;
        try {
            value = response.extract().path(keyString).toString();
            value = value.replaceAll("\\[", "").replaceAll("\\]", "");
        }
        catch (Exception e) {
            log("Failed to get value from JSON Response: " + e, "ERROR", "text");
        }

        return value;
    }

    //region <GET method>
    public ValidatableResponse getMethod(String endpoint, Map header) throws Exception {

        log("Method: GET\n---------------- URL ------------------\n"
                + RestAssured.baseURI + "" + RestAssured.basePath + endpoint, "INFO", "text");

        log("--------------- HEADERS ---------------\n" + header, "INFO", "text");

        response =
                given().
                        spec(requestSpec).
                        headers(header).
                when().
                        get(endpoint).
                then();

        log("--------------- RESPONSE ---------------\n", "INFO", "text");
        log(response.extract().body().asString(), "INFO", "json");

        return response;
    }
    //endregion
}
